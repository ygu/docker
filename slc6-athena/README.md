Athena-21.0.X Image Configuration (releases).
======================================

This configuration can be used to build an image providing a completely
standalone installation of an Athena/AtlasOffline-21.0.X release. 

Building an image for an existing numbered release can be done with:

```bash
docker build -t <username>/athena:21.0.41 --build-arg PROJECT=Athena --build-arg RELEASE=21.0.41 .
```


Athena-21.0 Image Configuration (nightlies)
======================================

This configuration can be used to build an image providing a completely
standalone installation of an Athena-21.0 nightly.

Building an image for an existing nightly can be done with:

```bash
docker build -t <username>/athena:21.0.42_2017-11-06T2150 --build-arg RELEASE=21.0.42 --build-arg TIMESTAMP=2017-11-06T2150 .
```


Examples
--------

You can find some pre-built images of both types at:  
[atlas/athena](https://hub.docker.com/r/atlas/athena/)  
[atlas/athena/tags](https://hub.docker.com/r/atlas/athena/tags/) 

Additional Setup
======================================

The following environment variable needs to be exported manually, and include the local squid servers (if any)
FRONTIER_SERVER
