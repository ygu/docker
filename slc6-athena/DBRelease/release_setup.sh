#
# Environment configuration file for the ATLAS user. Can be sourced to set up
# the athena release installed in the image.
#

# Set necessary variables to steer/complement AtlasSetup:
export SITEROOT=/usr
export TDAQ_RELEASE_BASE=/sw/atlas
export ATLAS_RELEASEDATA=/usr/atlas/offline/ReleaseData
unset FRONTIER_SERVER

export DBRELEASE=100.0.2
export DBRELEASE_INSTALLDIR=/cvmfs/atlas.cern.ch/repo/sw/database
export TNS_ADMIN=${DBRELEASE_INSTALLDIR}/DBRelease/${DBRELEASE}/oracle-admin
export DATAPATH=${DBRELEASE_INSTALLDIR}/DBRelease/${DBRELEASE}

source /opt/atlas/AtlasSetup/scripts/asetup.sh ${AtlasProject} --input /usr/atlas/.asetup

export CORAL_AUTH_PATH=${DBRELEASE_INSTALLDIR}/DBRelease/${DBRELEASE}/XMLConfig
export CORAL_DBLOOKUP_PATH=${DBRELEASE_INSTALLDIR}/DBRelease/${DBRELEASE}/XMLConfig

PATCH_SETUP=/home/atlas/build/x86_64-slc6-gcc62-opt/setup.sh
if [ -f "$PATCH_SETUP" ]; then
  echo "Including local patches"
  source $PATCH_SETUP
fi

source /opt/lcg/gcc/6.2.0binutils/x86_64-slc6/setup.sh
source /opt/lcg/binutils/2.28/x86_64-slc6/setup.sh
